import React, { useState } from 'react';
import Footer from './components/footer/Footer';
import Navbar from './components/navbar/Navbar';
import DetailsPage from './pages/detailsPage/DetailsPage';
import GamesList from './pages/gamesList/GamesList';
import Home from './pages/home/Home';
import LoginPage from './pages/loginPage/LoginPage';
import { views } from './services/constants';
function App() {
  const [currentView, setCurrentView] = useState(views.home);
  const [currentCard, setCurrentCard] = useState({});
  const [user, setUser] = useState(null);
  return (
    <div className='App'>
      {currentView !== views.login && (
        <>
          <Navbar setCurrentView={setCurrentView} user={user} />
          {currentView === views.home && (
            <Home setCurrentView={setCurrentView} />
          )}
          {currentView === views.games && (
            <GamesList
              setCurrentView={setCurrentView}
              setCurrentCard={setCurrentCard}
            />
          )}
          {currentView === views.single && (
            <DetailsPage currentCard={currentCard} user={user} />
          )}
          <Footer />
        </>
      )}
      {currentView === views.login && (
        <LoginPage setCurrentView={setCurrentView} setUser={setUser} />
      )}
    </div>
  );
}

export default App;
