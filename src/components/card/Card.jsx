import React from 'react';
import './card.scss';
import noImageFound from '../../assets/no-image-found.png';
import { views } from '../../services/constants';
const Card = ({ card, setCurrentCard, setCurrentView, className }) => {
  const cardImage = card.cover_art?.url;
  const handleCardClick = () => {
    setCurrentView(views.single);
    setCurrentCard(card);
  };
  return (
    <div className={`card ${className} `} onClick={handleCardClick}>
      <img src={card.cover_art ? cardImage : noImageFound} alt='cardImage' />
      <div className='card__info'>
        <div className='info__title'>
          <h4>{card.name}</h4>
        </div>

        <div className='info__footer'>
          <span>Genre: {card.genre.name}</span>
          <span>Price: ${card.price}</span>
        </div>
      </div>
    </div>
  );
};

export default Card;
