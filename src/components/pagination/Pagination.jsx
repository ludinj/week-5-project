import React, { useMemo } from 'react';
import { postsPerPage } from '../../services/constants';
import {
  BsFillArrowLeftCircleFill,
  BsFillArrowRightCircleFill,
} from 'react-icons/bs';
import './pagination.scss';

const Pagination = ({ totalPosts, setCurrentPage, currentPage }) => {
  const pages = useMemo(() => {
    const pages = [];
    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
      pages.push(i);
    }
    return pages;
  }, [totalPosts]);

  const handleArrowClick = (action) => {
    window.scroll(0, 0);
    if (action === 'minus' && currentPage > 1) {
      setCurrentPage((prev) => prev - 1);
    }
    if (action === 'plus' && pages.length > currentPage) {
      setCurrentPage((prev) => prev + 1);
    }
  };

  const handleButtonClick = (page) => {
    window.scroll(0, 0);
    setCurrentPage(page);
  };
  return (
    <div className='pagination'>
      {currentPage !== 1 && (
        <BsFillArrowLeftCircleFill
          className='arrow'
          onClick={() => handleArrowClick('minus')}
        />
      )}
      <div className='pages'>
        {pages.map((page, index) => {
          return (
            <button
              key={index}
              onClick={() => handleButtonClick(page)}
              className={page === currentPage ? 'active' : ''}
            >
              {page}
            </button>
          );
        })}
      </div>
      {currentPage !== pages.length && (
        <BsFillArrowRightCircleFill
          className='arrow'
          onClick={() => handleArrowClick('plus')}
        />
      )}
    </div>
  );
};

export default Pagination;
