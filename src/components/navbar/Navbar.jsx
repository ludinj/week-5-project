import React from 'react';
import './navbar.scss';
import brandImage from '../../assets/brand.png';
import { views } from '../../services/constants';
const Navbar = ({ setCurrentView, user }) => {
  return (
    <nav className='navbar'>
      <div className='navbar__content'>
        <div className='navbar__brand'>
          <img src={brandImage} alt='brand' />
        </div>
        <ul>
          <li className='nav__item' onClick={() => setCurrentView(views.home)}>
            Home
          </li>
          <li className='nav__item' onClick={() => setCurrentView(views.games)}>
            Games
          </li>
        </ul>
        {user ? (
          <span>{user.user.username}</span>
        ) : (
          <div className='login' onClick={() => setCurrentView('login')}>
            <p>Login</p>
          </div>
        )}
      </div>
    </nav>
  );
};
export default Navbar;
