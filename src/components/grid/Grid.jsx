import React from 'react';
import Card from '../card/Card';
import './grid.scss';
const Grid = ({ currentCards, setCurrentView, setCurrentCard }) => {
  return (
    <div className='grid__wrapper'>
      <div className='grid__content'>
        {currentCards.map((card) => (
          <Card
            key={card.id}
            card={card}
            className={'grid__card'}
            setCurrentCard={setCurrentCard}
            setCurrentView={setCurrentView}
          />
        ))}
      </div>
    </div>
  );
};

export default Grid;
