export const BASE_API_URL = 'https://trainee-gamerbox.herokuapp.com';
export const postsPerPage = 6;
export const DELAY = 1000;

export const views = {
  home: ' home',
  games: 'games',
  single: 'single',
  login: 'login',
};
