import React, { useState } from 'react';
import Grid from '../../components/grid/Grid';
import Pagination from '../../components/pagination/Pagination';
import { useFetch } from '../../hooks/useFetch';
import { BASE_API_URL, postsPerPage } from '../../services/constants';
import './gamesList.scss';
const GamesList = ({ setCurrentView, setCurrentCard }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const lastPostIndex = currentPage * postsPerPage;
  const firstPostIndex = lastPostIndex - postsPerPage;
  const { data, loading } = useFetch(`${BASE_API_URL}/games`);
  const currentCards = data?.slice(firstPostIndex, lastPostIndex);
  return (
    <div className='wrapper'>
      {loading && <h3>Loading...</h3>}
      {data && (
        <>
          <Grid
            currentCards={currentCards}
            setCurrentView={setCurrentView}
            setCurrentCard={setCurrentCard}
          />
          <Pagination
            totalPosts={data.length}
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
          />
        </>
      )}
    </div>
  );
};

export default GamesList;
