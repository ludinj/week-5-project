import React, { useState } from 'react';
import { BASE_API_URL, DELAY, views } from '../../services/constants';
import './loginPage.scss';
import { throttle } from '../../services/utils';
import { useCallback } from 'react';
const LoginPage = ({ setCurrentView, setUser }) => {
  const [inputData, setInputData] = useState({ identifier: '', password: '' });
  const [error, setError] = useState(null);

  const handleChange = (e) => {
    setInputData((pre) => ({ ...pre, [e.target.name]: e.target.value.trim() }));
  };

  const handleLogin = useCallback(async () => {
    try {
      const response = await fetch(`${BASE_API_URL}/auth/local`, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify(inputData),
      });
      if (response.status === 200) {
        const user = await response.json();
        setUser(user);
        setCurrentView(views.home);
      } else {
        const error = await response.json();
        const errorMessage = error.message[0].messages[0].message;
        setError(errorMessage);
      }
    } catch (error) {
      alert(error);
    }
  }, [inputData, setCurrentView, setUser]);

  const invokeThrottle = throttle(handleLogin, DELAY);

  return (
    <div className='login__wrapper'>
      <form>
        <h1>Login</h1>
        <div className='input__box'>
          <label htmlFor='identifier'>Username</label>
          <input type='text' onChange={handleChange} name='identifier' />
        </div>
        <div className='input__box'>
          <label htmlFor='password'>Password</label>
          <input type='password' onChange={handleChange} name='password' />
        </div>
        {error && <span>{error}</span>}
        <button type='button' onClick={invokeThrottle}>
          LOGIN
        </button>
      </form>
    </div>
  );
};

export default LoginPage;
