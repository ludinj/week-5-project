import React, { useEffect, useState } from 'react';
import { Comment } from '../../components/comment/Comment';
import { useFetch } from '../../hooks/useFetch';
import { useInputRef } from '../../hooks/useInputRef';
import { BASE_API_URL } from '../../services/constants';
import noImage from '../../assets/no-image-found.png';
import './detailsPage.scss';
const DetailsPage = ({ currentCard, user }) => {
  const [comments, setComments] = useState([]);
  const [isButtonVisible, setIsButtonVisible] = useState(false);
  const { data } = useFetch(
    `${BASE_API_URL}/games/${currentCard.id}/comments/?_limit=-1`
  );
  const inputRef = useInputRef();

  useEffect(() => {
    const sortedComments = data?.sort((a, b) => {
      return new Date(b.created_at) - new Date(a.created_at);
    });

    setComments(sortedComments);
  }, [data]);

  const handleChange = (e) => {
    inputRef.current.value = e.target.value;
    if (inputRef.current.value !== '') {
      setIsButtonVisible(true);
    } else {
      setIsButtonVisible(false);
    }
  };

  const postComment = async () => {
    if (user) {
      try {
        const response = await fetch(
          `${BASE_API_URL}/games/${currentCard.id}/comment`,
          {
            method: 'POST',
            headers: {
              Authorization: `Bearer ${user.jwt}`,
              'Content-type': 'application/json',
            },
            body: JSON.stringify({ body: inputRef.current.value }),
          }
        );
        const newComment = await response.json();
        setComments((pre) => [newComment, ...pre]);
        inputRef.current.value = '';
        setIsButtonVisible(false);
      } catch (error) {
        alert(error);
      }
    } else {
      alert('You must Login to comment');
    }
  };
  return (
    <div className='detailsPage__wrapper'>
      <div className='card__container'>
        <div className='image'>
          <img
            src={currentCard.cover_art ? currentCard.cover_art.url : noImage}
            alt='cardImage'
          />
        </div>
        <div className='details'>
          <h4>{currentCard.name}.</h4>
          <p>Release Year: {currentCard.release_year}</p>
          <p>Genre: {currentCard.genre.name}</p>
          <p>
            Release for:
            {currentCard.platforms.map((item, idx) => (
              <span key={idx}>{item.name}</span>
            ))}
          </p>
          <p>Price: ${currentCard.price}</p>
        </div>
      </div>
      <section className='comments__container'>
        <div className='comment__input'>
          <input
            type='text'
            placeholder='Agrega un comentario…'
            onChange={handleChange}
            ref={inputRef}
          />
          {isButtonVisible && (
            <button className='button' onClick={postComment}>
              Comentar
            </button>
          )}
        </div>
        <div className='comments'>
          {comments ? (
            comments.map((comment, idx) => (
              <Comment comment={comment} key={idx} />
            ))
          ) : (
            <h4>Loading..</h4>
          )}
        </div>
      </section>
    </div>
  );
};

export default DetailsPage;
