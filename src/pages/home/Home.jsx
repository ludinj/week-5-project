import React from 'react';
import './home.scss';
import minecraftBg from '../../assets/minecraft.jpg';
import brand from '../../assets/brand.png';
import { BsFillArrowRightCircleFill } from 'react-icons/bs';
const Home = ({ setCurrentView }) => {
  return (
    <div className='home__wrapper'>
      <img src={minecraftBg} alt='' />
      <div className='overlay'>
        <div className='info__card'>
          <img src={brand} alt='' />
          <p>
            Find and play the best games for you,{' '}
            <strong>Is time to have fun!</strong>
          </p>
          <span>We believe in community</span>
          <button onClick={() => setCurrentView('games-list')}>
            <BsFillArrowRightCircleFill />
            Explore games
          </button>
        </div>
      </div>
    </div>
  );
};

export default Home;
