import { useRef } from 'react';
export const useInputRef = () => {
  const inputRef = useRef(null);
  return inputRef;
};
